﻿Param
(
    [Parameter(Mandatory = $true)]
    [string]$secretText,
    [string]$filename
)

$salt = 'foobar'
$bytes = [System.Text.Encoding]::Unicode.GetBytes($secretText)
$iv = [System.Text.Encoding]::Unicode.GetBytes($salt)

Clear-Variable -Name 'secretText','salt'

Add-Type -AssemblyName System.Security
$encryptedBytes = [System.Security.Cryptography.ProtectedData]::Protect($bytes, $iv, 'LocalMachine')
$fileOutput = [System.Convert]::ToBase64String($encryptedBytes)
$fileOutput >> ($filename + "_DO_NOT_DELETE.txt")

rv -Name 'bytes','iv','encryptedBytes','fileOutput'