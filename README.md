# powershell_backup

My powershell backup scripts running with Windows Task Scheduler.

**Requirements:**

* [Restic](https://restic.net/)
* [Backblaze B2 storage](https://www.backblaze.com/b2/cloud-storage.html) (optional)
* [WinSCP](https://winscp.net/eng/index.php) (Local machine) (optional)
* [Putty](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)
* Remote server with Linux and public IP address. (optional)
* SMPT server on local machine. (optional)

For better data security every script create backup to local machine (another disk or SMB share in local network) and remote storage (Backblaze B2 storage or another server).

**File description:**
*vps_backup.ps1:*
* Upload files to another server via SFTP protocol. SFTP authentication is with password protected private key (created in PuttyGen.exe). 
* Upload provide WinSCP.
* Upload status with name of uploaded and not uploaded files or upload error is sending to e-mail(s).
* Password to private key is saved in encrypted text in file.

*restic_backup.ps1:*
* Create backup to local machine and Backblaze B2 storage.
* Passwords to local backup and remote server is saved in encrypted text in files.
* Backup errors is saved in files "LocalBackupErrors.txt" and "RemoteBackupErrors.txt".
* Script keep only 35 last backups, every olders automatically delete.

*encrypt_text.ps1*
* Script to generate file with encrypted password.
* You can change salt in variable "$salt". **THIS SALT YOU MUST WRITE TO SCRIPTS "vps\_backup.ps1" AND "restic\_backup.ps1" TO VARIABLE "$salt"!!!**
* Usage is "encrypt_text.ps1 <SECRET TEXT> <FILENAME>"
* Generated file have suffix "\_DO\_NOT\_DELETE.txt". 
* If you use script with parameters **"encrypt\_text.ps1 foo bar"** and **salt "test"** you create file with name "**bar_DO_NOT_DELETE.txt**" which contains encrypted password **foo** with salt "test".