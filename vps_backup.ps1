﻿param (
    $localPath = "c:\LocalPath\Backups\",
    $remoteBackupPath = "/mnt/RemoteBackup/",
    $localBackupPath = "D:\UploadedToVPS\" + "VPS\"
)

try
{
    # Password decrypt
    $salt = 'foobar' # Write your salt used in "encrypt_text.ps1"
    $ivector = [System.Text.Encoding]::Unicode.GetBytes($salt)
    Add-Type -AssemblyName System.Security
    $vps_pass = [System.Security.Cryptography.ProtectedData]::Unprotect(([Convert]::FromBase64String((Get-Content -Path C:\vps_pass.txt))), $ivector, 'LocalMachine')

    # Load WinSCP .NET assembly
    Add-Type -Path "c:\Program Files (x86)\WinSCP\WinSCPnet.dll"
  
    # Setup session options for SFTP transfer
    $sessionOptions = New-Object WinSCP.SessionOptions -Property @{
        Protocol = [WinSCP.Protocol]::Sftp
        HostName = "1.1.1.1" # VPS hostname or ip address
        UserName = "backup"
        PortNumber = 22     # Change if you use another SSH port 
        SecurePrivateKeyPassphrase = [System.Text.Encoding]::Unicode.GetString($vps_pass) | ConvertTo-SecureString -AsPlainText -Force
        SshPrivateKeyPath = "C:\id_ed25519.ppk" # Generate with PuttyGen.exe
        SshHostKeyFingerprint = "ssh-ed25519 255 ..." # from PuttyGen.exe
    }

    $session = New-Object WinSCP.Session

    try
    {
        # Connect
        $session.Open($sessionOptions)
        
        # Upload files, collect results
        cd $localPath
        $lines = Get-ChildItem -Name -File

        foreach ($line in $lines)
        {
            Write-Host "Uploading $line ..."
            
            if (($session.FileExists($remoteBackupPath + $line)) -eq $false){
                $transferResult = $session.PutFiles($localPath + $line, $remoteBackupPath)

                if ($transferResult.Transfers.Error -eq $Null)
                {
                    Write-Host "Upload of $($transferResult.Transfers.FileName) succeeded, moving to backup"
                    # Upload succeeded, move source file to backup
                    Move-Item $transferResult.Transfers.FileName $localBackupPath
                }
                else
                {
                    Write-Host "Upload of $($transferResult.Transfers.FileName) failed: $($transferResult.Transfers.Error.Message)"
                }
            }
            else
            {
                Move-Item ($localPath + $line), $remoteBackupPath
            }
        }
    }
    finally
    {
        # Disconnect, clean up
        $session.Dispose()

        try{
            # Send e-mail with upload status
            cd $localPath
            $notuploaded_files = Get-ChildItem -Name -File

            $From = "backup@backup.net"
            $To = "administrator@foo.bar"
            $Cc = ""
            $Subject = ("Backup status from  " + ((Get-Date -Format "dd.MM.yyyy").ToString()))
            $uploaded_files = $lines -join "`n"
            $notuploaded_files1 = $notuploaded_files -join "`n"
            $Body = ("Uploaded files to server: `n" + $uploaded_files + "`n`nNot uploaded files to server: `n" + $notuploaded_files1)
            $SMTPServer = "localhost"
            $SMTPPort = "25"
            Send-MailMessage -From $From -to $To -Cc $Cc -Subject $Subject -Body $Body -SmtpServer $SMTPServer -port $SMTPPort -Encoding UTF8 –DeliveryNotificationOption OnSuccess

            rv -Name 'lines','From','To','Cc','Subject','uploaded_files','notuploaded_files1','notuploaded_files','Body','SMTPServer','SMTPPort'
        }
        finally {      
            rv 'session'
        }
    }
}
catch
{
    Write-Host "Error: $($_.Exception.Message)"
    
    $From = "backup@backup.net"
    $To = "administrator@foo.bar"
    $Cc = ""
    $Subject = ("Error during backup from  " + ((Get-Date -Format "dd.MM.yyyy").ToString()))
    $Body = ("An error occurred during backup: `n" + $($_.Exception.Message))
    $SMTPServer = "localhost"
    $SMTPPort = "25"
    Send-MailMessage -From $From -to $To -Cc $Cc -Subject $Subject -Body $Body -SmtpServer $SMTPServer -port $SMTPPort -Encoding UTF8 –DeliveryNotificationOption OnSuccess

    rv -Name 'lines','From','To','Cc','Subject','uploaded_files','nenahrate_subory1','nenahrate_subory','Body','SMTPServer','SMTPPort'
}
    