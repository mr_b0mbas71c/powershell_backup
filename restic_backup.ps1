﻿# Password decrypting
$salt = 'foobar' # Write your salt used in "encrypt_text.ps1"
$ivector = [System.Text.Encoding]::Unicode.GetBytes($salt)
Add-Type -AssemblyName System.Security
# In "-Path" set path to file with encrypted text
$env:RESTIC_PASSWORD = [System.Text.Encoding]::Unicode.GetString([System.Security.Cryptography.ProtectedData]::Unprotect(([System.Convert]::FromBase64String((Get-Content -Path C:\DUbackup_pass.txt))), $ivector, 'LocalMachine'))
$env:B2_ACCOUNT_KEY = [System.Text.Encoding]::Unicode.GetString([System.Security.Cryptography.ProtectedData]::Unprotect(([System.Convert]::FromBase64String((Get-Content -Path C:\B2_applicationKey.txt))), $ivector, 'LocalMachine'))
$env:B2_ACCOUNT_ID = [System.Text.Encoding]::Unicode.GetString([System.Security.Cryptography.ProtectedData]::Unprotect(([System.Convert]::FromBase64String((Get-Content -Path C:\B2_applicationKeyId.txt))), $ivector, 'LocalMachine'))


# Create backup to local disk machine
restic backup "C:\Backed_up_folder" # 
# Errors write in LocalBackupErrors.txt file
if ((restic check) -notlike '*no errors*') {
    (restic check) >> C:\LocalBackupErrors.txt
} 
restic unlock

# Keep only 35 last backups.
restic forget --keep-last 35 --prune

# Create remote backup on Backblaze B2 storage.
restic -r b2:BucketName:FolderInBucketName backup "C:\Backed_up_folder"
# Errors write in RemoteBackupErrors.txt file
if ((restic -r b2:BucketName:FolderInBucketName check) -notlike '*no errors*') {
    (restic -r b2:BucketName:FolderInBucketName check) >> C:\RemoteBackupErrors.txt
}
restic -r b2:BucketName:FolderInBucketName unlock
# Keep only 35 last backups
restic -r b2:BucketName:FolderInBucketName forget --keep-last 35 --prune